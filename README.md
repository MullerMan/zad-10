#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main()
{
	FILE *source;
	srand(time(NULL));
	int a, b, c, wynik;
	source = fopen("DANE.DAT", "w");
	for(int x = 0; x <= 15; x++)				// wpisywanie do pliku
		fprintf(source, "%i\n", rand()%11);
	fclose(source);
	
	
	source = fopen("DANE.DAT", "r");			// czytanie wynikow z pliku
	for(int x = 0; x < 5; x++)
	{
		fscanf(source, "%i", &a);
		fscanf(source, "%i", &b);
		fscanf(source, "%i", &c);
		wynik = pow(a, 2) + b - c;	
		printf("%i^2 + %i - %i = %i ", a, b, c, wynik);
		printf("\n");
	};
	fclose(source);
}